import * as factory from "factory.ts";
import faker from "faker";

import { Address } from "../../../src/domain/address";

export const anyAddress = factory.Sync.makeFactory<Address>({
    place: faker.random.alpha(),
    complement: faker.random.alpha(),
    district: faker.random.alpha(),
    locality: faker.random.alpha(),
    state: faker.random.alpha(),
    cep: faker.random.alpha(),
    infos: new Map(),
});
