import "reflect-metadata";
import { mock } from "jest-mock-extended";

import { anyAddress } from "../../__fixture__/domain/address.data-builder";
import { Address } from "../../../src/domain/address";
import { CepRepositoryGateway } from "../../../src/gateways/cep-repository.gateway";
import { GetAddressUseCase } from "../../../src/use-cases/get-address.usecase";

describe("Unit Tests of GetAddressUseCase", () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    const cep = "anyCep";
    const addressExistent = anyAddress.build();

    it("Get Address with success", async () => {
        const mockedCepRepositoryGateway = mock<CepRepositoryGateway>();
        mockedCepRepositoryGateway.getAddress.calledWith(cep).mockResolvedValue(addressExistent);

        const getAddressUseCase = new GetAddressUseCase(mockedCepRepositoryGateway);

        const addressPromise = getAddressUseCase.get(cep);

        addressPromise.then((addressFound: Address) => {
            expect(addressExistent.place).toEqual(addressFound.place);
            expect(addressExistent.complement).toEqual(addressFound.complement);
            expect(addressExistent.district).toEqual(addressFound.district);
            expect(addressExistent.locality).toEqual(addressFound.locality);
            expect(addressExistent.state).toEqual(addressFound.state);
            expect(addressExistent.cep).toEqual(addressFound.cep);
        });
    });
});
