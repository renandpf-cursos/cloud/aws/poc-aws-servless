// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { injectable, inject } from "inversify";
import { Address } from "src/domain/address";

import { AddressRepositoryGateway } from "@gateways/address-repository.gateway";

import { logger } from "@configs/logger";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { GATEWAY_TYPES } from "../configs/container/gateway-types";

@injectable()
// eslint-disable-next-line @typescript-eslint/no-unused-vars
export class SaveAddressUseCase {
    public static TARGET_NAME = "SaveAddressUseCase";
    constructor(
        @inject(GATEWAY_TYPES.addressRepositoryGateway) private addressRepositoryGateway: AddressRepositoryGateway
    ) {}

    public save(address: Address): Promise<any> {
        logger.info("Start address={}", address);

        const id = this.addressRepositoryGateway.save(address);

        logger.info("end={}", id);
        return id;
    }
}
