// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { injectable, inject } from "inversify";
import { Address } from "src/domain/address";

import { logger } from "@configs/logger";

import { GetAddressUseCase } from "./get-address.usecase";
import { SaveAddressUseCase } from "./save-address.usecase";

@injectable()
// eslint-disable-next-line @typescript-eslint/no-unused-vars
export class ProcessAddressOrchestrator {
    public static TARGET_NAME = "ProcessAddressOrchestrator";
    constructor(
        @inject(GetAddressUseCase.TARGET_NAME) private getAddressUseCase: GetAddressUseCase,
        @inject(SaveAddressUseCase.TARGET_NAME) private saveAddressUseCase: SaveAddressUseCase
    ) {}

    public async process(cep: string) {
        logger.info("Start cep={}", cep);

        const address: Address = await this.getAddressUseCase.get(cep);
        await this.saveAddressUseCase.save(address);
        logger.info("end");
    }
}
