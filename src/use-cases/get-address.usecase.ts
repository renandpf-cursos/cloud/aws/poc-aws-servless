// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { injectable, inject } from "inversify";
import { Address } from "src/domain/address";

import { CepRepositoryGateway } from "@gateways/cep-repository.gateway";

import { logger } from "@configs/logger";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { GATEWAY_TYPES } from "../configs/container/gateway-types";

@injectable()
// eslint-disable-next-line @typescript-eslint/no-unused-vars
export class GetAddressUseCase {
    public static TARGET_NAME = "GetAddressUseCase";
    constructor(@inject(GATEWAY_TYPES.cepRepositoryGateway) private cepRepositoryGateway: CepRepositoryGateway) {}

    public get(cep: string): Promise<Address> {
        logger.info("Start cep={}", cep);

        const addressPromisse = this.cepRepositoryGateway.getAddress(cep);

        logger.info("end={}", addressPromisse);
        return addressPromisse;
    }
}
