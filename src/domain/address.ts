export class Address {
    public readonly place: string;
    public readonly complement: string;
    public readonly district: string;
    public readonly locality: string;
    public readonly state: string;
    public readonly cep: string;
    public readonly infos: Map<string, any>;

    constructor(
        place: string,
        complement: string,
        district: string,
        locality: string,
        state: string,
        cep: string,
        infos: Map<string, any>
    ) {
        this.place = place;
        this.complement = complement;
        this.district = district;
        this.locality = locality;
        this.state = state;
        this.cep = cep;
        this.infos = infos;
    }
}
