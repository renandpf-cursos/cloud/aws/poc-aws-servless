import * as bodyParser from "body-parser";
import { interfaces } from "inversify-express-utils";

export class ExpressConfigFactory {
    public static create(): interfaces.ConfigFunction {
        return (app: any) => {
            app.use(bodyParser.json());
        };
    }
}
