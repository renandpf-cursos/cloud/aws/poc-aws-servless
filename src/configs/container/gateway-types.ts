const LABEL_TYPES = {
    cepRepositoryGateway: "cepRepositoryGateway",
    addressRepositoryGateway: "addressRepositoryGateway",
};

export const GATEWAY_TYPES = {
    cepRepositoryGateway: Symbol.for(LABEL_TYPES.cepRepositoryGateway),
    addressRepositoryGateway: Symbol.for(LABEL_TYPES.addressRepositoryGateway),
};
