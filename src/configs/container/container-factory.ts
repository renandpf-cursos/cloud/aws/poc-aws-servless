import { Container } from "inversify";

import { CepRepositoryGateway } from "@gateways/cep-repository.gateway";
import { ViaCepGateway } from "@gateways/http/viacep/viacep.gateway";

import { SaveAddressUseCase } from "@usecases/save-address.usecase";

import { GATEWAY_TYPES } from "../../configs/container/gateway-types";
import { GetAddressUseCase } from "../../use-cases/get-address.usecase";

export class ContainerFactory {
    public static create(): Container {
        const container: Container = new Container();

        container.bind<GetAddressUseCase>(GetAddressUseCase.TARGET_NAME).to(GetAddressUseCase).inSingletonScope();
        container.bind<SaveAddressUseCase>(SaveAddressUseCase.TARGET_NAME).to(SaveAddressUseCase).inSingletonScope();

        container.bind<CepRepositoryGateway>(GATEWAY_TYPES.cepRepositoryGateway).to(ViaCepGateway).inSingletonScope();

        return container;
    }
}
