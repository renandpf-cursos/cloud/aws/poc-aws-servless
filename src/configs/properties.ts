// eslint-disable-next-line @typescript-eslint/no-var-requires
require("custom-env").env(true, "environments/");

export const properties = {
    env: process.env.APP_ENV,
    appName: "poc-aws-servless",
    baseUrl: {
        v1: "/poc-aws/v1",
    },
    port: 8000,
};
