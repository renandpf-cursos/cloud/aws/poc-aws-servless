// https://docs.aws.amazon.com/lambda/latest/dg/typescript-handler.html
// https://github.com/andrea-lascola/serverless-inversify-example/blob/master/src/handler.ts

import { Context, APIGatewayProxyResult, APIGatewayEvent } from "aws-lambda";

import { ProcessAddressOrchestrator } from "@usecases/process-address.orchestrator";

import { ContainerFactory } from "@configs/container/container-factory";
import { logger } from "@configs/logger";

export const handler = async (event: APIGatewayEvent, context: Context): Promise<APIGatewayProxyResult> => {
    logger.info("Start event={}, context={}", event, context);
    let result = null;
    try {
        const container = ContainerFactory.create();
        const processAddressOrchestrator: ProcessAddressOrchestrator = container.get<ProcessAddressOrchestrator>(
            Symbol.for(ProcessAddressOrchestrator.TARGET_NAME)
        );
        const dataJson = mapper(event);

        await processAddressOrchestrator.process(dataJson.cep);
        result = getResultSuccess();
    } catch (err) {
        logger.error(err);
        result = getResultError(err);
    }
    logger.info("End result={}", result);
    return result;
};

function getResultError(err: Error) {
    return {
        statusCode: 500,
        body: JSON.stringify({ message: "error: " + err }),
    };
}

function getResultSuccess() {
    return {
        statusCode: 200,
        body: JSON.stringify({ message: "Process done successfully!" }),
    };
}

function mapper(event: APIGatewayEvent) {
    const eventJson = JSON.parse(event.body);
    const data = {
        cep: eventJson.cep,
    };
    return data;
}
