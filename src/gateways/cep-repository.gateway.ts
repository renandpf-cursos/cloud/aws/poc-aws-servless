import { Address } from "src/domain/address";

export interface CepRepositoryGateway {
    getAddress(cep: string): Promise<Address>;
}
