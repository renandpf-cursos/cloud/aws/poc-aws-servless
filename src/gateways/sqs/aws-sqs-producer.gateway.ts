import { Address } from "src/domain/address";

import { AddressRepositoryGateway } from "@gateways/address-repository.gateway";

export class AwsSqsProducerGateway implements AddressRepositoryGateway {
    save(address: Address): Promise<any> {
        console.log(address);
        console.info("Method not implemented.");
        return Promise.resolve(
            new Address(
                "Any Place",
                "Any complement",
                "any district",
                "any locality",
                "any state",
                "any cep",
                new Map()
            )
        );
    }
}
