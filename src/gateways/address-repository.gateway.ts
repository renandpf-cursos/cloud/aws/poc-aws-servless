import { Address } from "src/domain/address";

export interface AddressRepositoryGateway {
    save(address: Address): Promise<any>;
}
