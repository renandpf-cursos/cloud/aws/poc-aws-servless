"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProcessAddressOrchestrator = void 0;
const inversify_1 = require("inversify");
const logger_1 = require("@configs/logger");
const get_address_usecase_1 = require("./get-address.usecase");
const save_address_usecase_1 = require("./save-address.usecase");
let ProcessAddressOrchestrator = class ProcessAddressOrchestrator {
    constructor(getAddressUseCase, saveAddressUseCase) {
        this.getAddressUseCase = getAddressUseCase;
        this.saveAddressUseCase = saveAddressUseCase;
    }
    async process(cep) {
        logger_1.logger.info("Start cep={}", cep);
        const address = await this.getAddressUseCase.get(cep);
        await this.saveAddressUseCase.save(address);
        logger_1.logger.info("end");
    }
};
ProcessAddressOrchestrator.TARGET_NAME = "ProcessAddressOrchestrator";
ProcessAddressOrchestrator = __decorate([
    (0, inversify_1.injectable)(),
    __param(0, (0, inversify_1.inject)(get_address_usecase_1.GetAddressUseCase.TARGET_NAME)),
    __param(1, (0, inversify_1.inject)(save_address_usecase_1.SaveAddressUseCase.TARGET_NAME)),
    __metadata("design:paramtypes", [get_address_usecase_1.GetAddressUseCase,
        save_address_usecase_1.SaveAddressUseCase])
], ProcessAddressOrchestrator);
exports.ProcessAddressOrchestrator = ProcessAddressOrchestrator;
//# sourceMappingURL=process-address.orchestrator.js.map