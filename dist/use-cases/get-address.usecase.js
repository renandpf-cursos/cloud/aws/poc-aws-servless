"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetAddressUseCase = void 0;
const inversify_1 = require("inversify");
const gateway_types_1 = require("../configs/container/gateway-types");
const logger_1 = require("@configs/logger");
let GetAddressUseCase = class GetAddressUseCase {
    constructor(cepRepositoryGateway) {
        this.cepRepositoryGateway = cepRepositoryGateway;
    }
    get(cep) {
        logger_1.logger.info("Start cep={}", cep);
        const addressPromisse = this.cepRepositoryGateway.getAddress(cep);
        logger_1.logger.info("end={}", addressPromisse);
        return addressPromisse;
    }
};
GetAddressUseCase.TARGET_NAME = "GetAddressUseCase";
GetAddressUseCase = __decorate([
    (0, inversify_1.injectable)(),
    __param(0, (0, inversify_1.inject)(gateway_types_1.GATEWAY_TYPES.cepRepositoryGateway)),
    __metadata("design:paramtypes", [Object])
], GetAddressUseCase);
exports.GetAddressUseCase = GetAddressUseCase;
//# sourceMappingURL=get-address.usecase.js.map