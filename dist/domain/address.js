"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Address = void 0;
class Address {
    constructor(place, complement, district, locality, state, cep, infos) {
        this.place = place;
        this.complement = complement;
        this.district = district;
        this.locality = locality;
        this.state = state;
        this.cep = cep;
        this.infos = infos;
    }
}
exports.Address = Address;
//# sourceMappingURL=address.js.map