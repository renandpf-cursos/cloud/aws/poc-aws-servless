"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContainerFactory = void 0;
const inversify_1 = require("inversify");
const viacep_gateway_1 = require("@gateways/http/viacep/viacep.gateway");
const save_address_usecase_1 = require("@usecases/save-address.usecase");
const gateway_types_1 = require("../../configs/container/gateway-types");
const get_address_usecase_1 = require("../../use-cases/get-address.usecase");
class ContainerFactory {
    static create() {
        const container = new inversify_1.Container();
        container.bind(get_address_usecase_1.GetAddressUseCase.TARGET_NAME).to(get_address_usecase_1.GetAddressUseCase).inSingletonScope();
        container.bind(save_address_usecase_1.SaveAddressUseCase.TARGET_NAME).to(save_address_usecase_1.SaveAddressUseCase).inSingletonScope();
        container.bind(gateway_types_1.GATEWAY_TYPES.cepRepositoryGateway).to(viacep_gateway_1.ViaCepGateway).inSingletonScope();
        return container;
    }
}
exports.ContainerFactory = ContainerFactory;
//# sourceMappingURL=container-factory.js.map