"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GATEWAY_TYPES = void 0;
const LABEL_TYPES = {
    cepRepositoryGateway: "cepRepositoryGateway",
    addressRepositoryGateway: "addressRepositoryGateway",
};
exports.GATEWAY_TYPES = {
    cepRepositoryGateway: Symbol.for(LABEL_TYPES.cepRepositoryGateway),
    addressRepositoryGateway: Symbol.for(LABEL_TYPES.addressRepositoryGateway),
};
//# sourceMappingURL=gateway-types.js.map