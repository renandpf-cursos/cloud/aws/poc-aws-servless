"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.properties = void 0;
require("custom-env").env(true, "environments/");
exports.properties = {
    env: process.env.APP_ENV,
    appName: "poc-aws-servless",
    baseUrl: {
        v1: "/poc-aws/v1",
    },
    port: 8000,
};
//# sourceMappingURL=properties.js.map