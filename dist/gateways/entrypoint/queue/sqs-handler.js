"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.handler = void 0;
const logger_1 = require("@configs/logger");
const handler = async (event, context) => {
    logger_1.logger.info("Start event={}, context={}", event, context);
    let result = null;
    try {
        const processAddressOrchestrator = null;
        const dataJson = mapper(event);
        await processAddressOrchestrator.process(dataJson.cep);
        result = getResultSuccess();
    }
    catch (err) {
        logger_1.logger.error(err);
        result = getResultError(err);
    }
    logger_1.logger.info("End result={}", result);
    return result;
};
exports.handler = handler;
function getResultError(err) {
    return {
        statusCode: 500,
        body: JSON.stringify({ message: "error: " + err }),
    };
}
function getResultSuccess() {
    return {
        statusCode: 200,
        body: JSON.stringify({ message: "Process done successfully!" }),
    };
}
function mapper(event) {
    const eventJson = JSON.parse(event.body);
    const data = {
        cep: eventJson.cep,
    };
    return data;
}
//# sourceMappingURL=sqs-handler.js.map